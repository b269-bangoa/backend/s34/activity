const express = require("express");

const app = express();

const port = 3000;

let users = [];

app.use(express.json());

app.use(express.urlencoded({extended: true}));

//GET /home route
app.get("/home", (req,res) => {
	res.send("Welcome to the home page.")
});

//Add user
app.post("/items", (req, res) => {
		if( req.body.username !=="" && req.body.password !=="") {
			users.push(req.body)
			res.send(`User ${req.body.username} successfully registered`)
		} else {
			res.send('Please enter valid username or password!')
		}
});

//GET /items route
app.get("/items", (req,res) => {
	res.send(users)
});
let message;

//DELETE delete-item route
app.delete("/delete-item", (req, res) => {
	for (let i = 0; i< users.length; i++){
		if(req.body.username == users[i].username){
			delete users[i];

			message = `User ${req.body.username} has been deleted`;
		break;
		} else {
			message = "User does not exist!"
		}
	}
	res.send(message);
})



app.listen(port, () => console.log(`Server running at ${port}`));